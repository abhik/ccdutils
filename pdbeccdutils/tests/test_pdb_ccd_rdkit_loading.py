#!/usr/bin/env python
# software from PDBe: Protein Data Bank in Europe; http://pdbe.org
#
# Copyright 2017 EMBL - European Bioinformatics Institute
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
#
import unittest

from nose.tools import assert_equals, assert_not_equals, assert_almost_equals

from pdbeccdutils.pdb_chemical_components_rdkit import PdbChemicalComponentsRDKit
from pdbeccdutils.tests.tst_utilities import cif_filename, supply_list_of_sample_cifs


def test_hard_code_cmo():
    cmo = PdbChemicalComponentsRDKit(cif_parser='test_hard_code_cmo')
    yield assert_equals, 'CMO', cmo.chem_comp_id, 'chem_comp_id'
    yield assert_equals, 'CARBON MONOXIDE', cmo.chem_comp_name, 'chem_comp_name'
    yield assert_equals, 'UGFAIRIUMAVXCW-UHFFFAOYSA-N', cmo.inchikey, 'chem_inchikey'
    # rdkit should be able to get an inchikey that is the same that read from the CCD cif file
    yield assert_equals, 2, cmo.rwmol_original.GetNumAtoms(), 'rdkit_mol.GetNumAtoms() should give 2'
    yield assert_equals, cmo.inchikey, cmo.inchikey_from_rdkit, 'inchikey from cif file should match the rdkit inchikey'


def test_load_eoh_from_cif():
    eoh = PdbChemicalComponentsRDKit(file_name=cif_filename('EOH'))
    yield assert_equals, 'EOH', eoh.chem_comp_id, 'chem_comp_id'
    yield assert_equals, eoh.inchikey, eoh.inchikey_from_rdkit, 'inchikey from cif file should match the rdkit inchikey'


def test_inchikey_match_for_all_sample_cifs():
    for ciffile in supply_list_of_sample_cifs():
        pdb_cc = PdbChemicalComponentsRDKit(file_name=ciffile)
        inchikey_from_rdkit = pdb_cc.inchikey_from_rdkit
        if pdb_cc.chem_comp_id in ('CDL', 'ASX', '7OM'):
            yield assert_not_equals, pdb_cc.inchikey, inchikey_from_rdkit, \
                'know inchikeys do not match for ' + pdb_cc.chem_comp_id
        else:
            yield assert_equals, pdb_cc.inchikey, inchikey_from_rdkit, \
                'check inchikeys match for ' + pdb_cc.chem_comp_id


def test_xyz_2d():
    eoh = PdbChemicalComponentsRDKit(file_name=cif_filename('EOH'))
    yield assert_equals, 3, len(eoh.xyz_2d_no_hydrogen), 'EOH length eoh.xyz_2d_no_hydrogen'
    yield assert_equals, 0., eoh.xyz_2d_no_hydrogen[0][0], 'EOH length eoh.xyz_2d_no_hydrogen first atom x'
    yield assert_almost_equals, 0.5, eoh.xyz_2d_no_hydrogen[0][1], 7, 'EOH length eoh.xyz_2d_no_hydrogen first atom y'
    yield assert_equals, 9, len(eoh.xyz_2d)


def test_torsion_calculation():
    gol = PdbChemicalComponentsRDKit(file_name=cif_filename('GOL'))
    yield assert_equals, 'GOL', gol.chem_comp_id, 'chem_comp_id'
    yield assert_almost_equals, 65.0067, gol.calculate_torsion( atom_indices=(1, 0, 2, 3), ideal=True),\
          4, 'O1-C1-C2-O2 torsion angle in degrees (to 4 places - cf to coot)'


class DummyTestCaseSoPycharmRecognizesNoseTestsAsTests(unittest.TestCase):
    pass
