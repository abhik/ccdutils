# software from PDBe: Protein Data Bank in Europe; http://pdbe.org
#
# Copyright 2017 EMBL - European Bioinformatics Institute
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
import errno
import shutil
import os


def create_directory_using_mkdir_unless_it_exists(path, clean_existing=False):
    if clean_existing:
        if os.path.isdir(path):
            shutil.rmtree(path)
    try:
        os.mkdir(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def data_dir():
    """
   directory of the tests directory where this python file is.

    Returns:
        str the name of the directory is

    """
    this_dir = os.path.dirname(os.path.abspath(__file__))
    this_dir = os.path.join(this_dir, 'data')
    return this_dir


default_fragment_library_file_path = os.path.join(data_dir(), 'fragment_library.tsv')
ring2dtemplates_file_path = os.path.join(data_dir(), 'ring2dtemplates.sdf')


